---
title: "Rocks"
description: "The idea of this page is inspired by the \"[Stuff that rocks](https://suckless.org/rocks/)\" page of suckless.org."
layout: default
---

<html lang="en">
  <head>
    <meta http-equiv="Refresh" content="0; url='https://tesk.page/rocks'"/>
  </head>
</html>
