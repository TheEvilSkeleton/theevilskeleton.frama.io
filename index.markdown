---
title: TheEvilSkeleton
description: "Welcome to my personal website! I talk about myself, computers, GNU/Linux ecosystems, open-source, and other topics related to software."
layout: default
---

<html lang="en">
  <head>
    <meta http-equiv="Refresh" content="0; url='https://tesk.page'"/>
  </head>
</html>
