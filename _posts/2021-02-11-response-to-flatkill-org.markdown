---
title: "Access freenode using Matrix clients"
layout: default
title: "Response to flatkill.org"
description: "A lot of flatkill.org's statements are made to incite fear in the Linux community. Given that all Flatpak packages and application build scripts are available and able to be edited by anyone, the appropriate response is to educate on why this is a problem, and then fix it. The way that flatkill.org approached this issue says a lot."
---

<html lang="en">
  <head>
    <meta http-equiv="Refresh" content="0; url='https://tesk.page/2021/02/11/response-to-flatkill-org.html'"/>
  </head>
</html>
