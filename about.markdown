---
title: "About Me"
description: "My name is Hari Rana. I’m also known as TheEvilSkeleton, TheEvilSkely, Skelly, and Tesk. I am an information activist, which means I promote free and accessible information to the general population, like free and open source software, open research, open science, etc. I am 19 years old, and I am very passionate about computers."
layout: default
---

<html lang="en">
  <head>
    <meta http-equiv="Refresh" content="0; url='https://tesk.page/about'"/>
  </head>
</html>
